import json
import csv
from datetime import datetime


class Validator:
    """
    Basic class prepared for validating input or output paths
    """
    def arg_validator(self, input_variable):
        """
        Basic method prepared for validating input or output path
        :param input_variable: str - path to validate
        """
        raise NotImplementedError()


class JSONValidator(Validator):
    """
    JSON Validator class prepared for validating input path
    """
    def arg_validator(self, json_path):
        """
        Method prepared for validating input path
        :param json_path: str - input path to validate
        :return: None
        """
        if json_path is None or not json_path.endswith('.json'):
            print("Error in JSON path!! Please try again...")
            exit()

        return


class CSVValidator(Validator):
    """
    CSV Validator class prepared for validating output path
    """
    def arg_validator(self, csv_path):
        """
        Method prepared for validating output path
        :param csv_path: str - output path to validate
        :return: None
        """
        if csv_path is None or not csv_path.endswith('.csv'):
            print("Error in CSV path!! Please try again...")
            exit()

        return


class JSONHandler:
    """
    Class to handle JSON data
    """
    def __init__(self, file_path):
        self.file_path = file_path

    def json_load(self):
        """
        Method to load JSON data
        :return: list of dictionaries associated to JSON data
        """
        with open(self.file_path, 'r') as fp:
            return json.load(fp)


class CSVHandler:
    """
    Class to handle CSV data
    """
    def __init__(self, file_path):
        self.file_path = file_path

    def write(self, json_input):
        """
        Method to write JSON data in CSV output
        :param json_input: list - list of dictionaries that includes JSON data
        :return:
        """
        with open(self.file_path, 'w', newline='') as fp:
            csv_write = csv.DictWriter(fp, fieldnames=["id", "name", "test_result", "start_date", "end_date"])
            csv_write.writeheader()
            csv_write.writerows(json_input)


class MetricsCalculator:
    """
    Basic class to calculate metrics
    """

    def __init__(self, input_data):
        self.input_data = input_data

    def total_test_cases(self):
        """
        Method to calculate total test cases
        :return: str - quantity of test cases
        """
        return str(len(self.input_data))

    def pass_test_cases(self):
        """
        Method to calculate passed test cases
        :return: str - quantity of passed test cases
        """
        pass_test_cases = 0

        for element in self.input_data:
            if element['test_result'] == 'PASS':
                pass_test_cases += 1

        return str(pass_test_cases)

    def fail_test_cases(self):
        """
        Method to calculate failed test cases
        :return: str - quantity of failed test cases
        """
        fail_test_cases = 0

        for element in self.input_data:
            if element['test_result'] == 'FAIL':
                fail_test_cases += 1

        return str(fail_test_cases)

    def average_execution_time(self):
        """
        Method to calculate average execution time in test cases
        :return: str - average execution time in format HH:MM:SS
        """
        average_time = 0

        # Calculating differences
        for element in self.input_data:
            start_date = datetime.strptime(element['start_date'], "%d-%m-%Y %H:%M:%S")
            end_date = datetime.strptime(element['end_date'], "%d-%m-%Y %H:%M:%S")
            execution_time = end_date - start_date
            average_time += execution_time.total_seconds()

        # Calculating average
        average_time /= len(self.input_data)

        return self.formatting_time(average_time)

    @staticmethod
    def formatting_time(average_time):
        """
        Inner method to format time
        :param average_time: int - date time in seconds
        :return: str - datetime in format HH:MM:SS
        """
        minutes, seconds = divmod(average_time, 60)
        hours, minutes = divmod(minutes, 60)

        # Casting
        hours = int(hours)
        minutes = int(minutes)
        seconds = int(seconds)

        # Formatting as string
        if hours < 10:
            hours = "0" + str(hours)
        else:
            hours = str(hours)

        if minutes < 10:
            minutes = "0" + str(minutes)
        else:
            minutes = str(minutes)

        if seconds < 10:
            seconds = "0" + str(seconds)
        else:
            seconds = str(seconds)

        return hours + ":" + minutes + ":" + seconds

    def minimum_execution_time(self):
        """
        Method to calculate minimum execution time in test cases
        :return: str - minimum execution time in format HH:MM:SS
        """
        minimum_time = None

        # Calculating differences
        for element in self.input_data:
            start_date = datetime.strptime(element['start_date'], "%d-%m-%Y %H:%M:%S")
            end_date = datetime.strptime(element['end_date'], "%d-%m-%Y %H:%M:%S")
            execution_time = end_date - start_date

            # Including first minimum_time
            if minimum_time is None:
                minimum_time = execution_time

            # Checking possible minimum_time
            if execution_time < minimum_time:
                minimum_time = execution_time

        return self.formatting_time(minimum_time.total_seconds())

    def maximum_execution_time(self):
        """
        Method to calculate maximum execution time in test cases
        :return: str - maximum execution time in format HH:MM:SS
        """
        maximum_time = None

        # Calculating differences
        for element in self.input_data:
            start_date = datetime.strptime(element['start_date'], "%d-%m-%Y %H:%M:%S")
            end_date = datetime.strptime(element['end_date'], "%d-%m-%Y %H:%M:%S")
            execution_time = end_date - start_date

            # Including first maximum_time
            if maximum_time is None:
                maximum_time = execution_time

            # Checking possible maximum_time
            if execution_time > maximum_time:
                maximum_time = execution_time

        return self.formatting_time(maximum_time.total_seconds())


class MetricsGenerator(MetricsCalculator):
    """
    Extending MetricsCalculator, this class allows to calculate and present output data in terminal
    """
    def __init__(self, input_data):
        super().__init__(input_data)
        self.metrics = {}

    def generator(self):
        """
        Calculate all required metrics calculation
        :return:
        """
        self.metrics['Total test cases'] = self.total_test_cases()
        self.metrics['Pass test cases'] = self.pass_test_cases()
        self.metrics['Fail test cases'] = self.fail_test_cases()
        self.metrics['Average execution time'] = self.average_execution_time()
        self.metrics['Minimum execution time'] = self.minimum_execution_time()
        self.metrics['Maximum execution time'] = self.maximum_execution_time()

        return

    def present_data(self):
        """
        Presents data in output terminal
        :return:
        """
        for element in self.metrics:
            print(element + ": " + self.metrics[element])

        return
