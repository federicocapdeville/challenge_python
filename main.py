import argparse
from collector import JSONHandler, CSVHandler, MetricsGenerator, JSONValidator, CSVValidator


if __name__ == "__main__":
    # Handle input via terminal
    parser = argparse.ArgumentParser(description="Python challenge")
    parser.add_argument('--json_path', type=str, help='Insert JSON input path')
    parser.add_argument('--csv_path', type=str, help='Insert CSV output path')
    args = parser.parse_args()

    # Collect paths
    json_path = args.json_path
    csv_path = args.csv_path

    # Validating input / output paths
    my_json_validator = JSONValidator()
    my_json_validator.arg_validator(json_path)
    my_csv_validator = CSVValidator()
    my_csv_validator.arg_validator(csv_path)

    # Collecting the JSON data
    json_handler = JSONHandler(json_path)
    json_data = json_handler.json_load()

    # Calculating metrics
    metrics_calculator = MetricsGenerator(json_data)
    metrics_calculator.generator()

    # Exporting to CSV data associated
    csv_handler = CSVHandler(csv_path)
    csv_handler.write(json_data)

    # Presenting metrics data on terminal
    metrics_calculator.present_data()
