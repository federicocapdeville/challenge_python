# Challenge: Python Solution for Test Results Analysis

### Challenge Description: 

Assume that you are given a JSON file containing the results of an executed test case. Your task is to create a Python program that processes this JSON data, converts it into a CSV file format, exports it, and generates various metrics based on the test results.

---

### Requirements:

- **Script requirements:** The presented solution must work successfully without requiring any edits to the script. Candidates must provide clear instructions on how to execute the script and specify any requirements it has.

- **JSON Format Assumption:** Candidates can assume any JSON format/structure for their solution, but they need to explicitly mention what the assumed JSON format content is in their code documentation or comments.

- **CSV Export:** Create a CSV file from the parsed data that includes details such as test case names, pass/fail status, execution times, and timestamps.

- **Export Functionality:** Implement a mechanism to export the generated CSV file to a specified location on the filesystem.

- **Metrics Calculation:** Calculate and display the following metrics based on the test results:
    - Total number of test cases executed.
    - Number of test cases passed. 
    - Number of test cases failed.
    - Average execution time for all test cases.
    - Minimum execution time among all test cases.
    - Maximum execution time among all test cases.

- **Code Quality:** Ensure that the code is well-formatted, follows coding conventions, and is easy to read and understand.

- **Modularity and SOLID Principles:** Design the code in a modular way, adhering to SOLID principles, to make it maintainable and extensible.

---

### Evaluation Criteria:

Candidates will be evaluated based on their ability to:

- Parse JSON data into Python objects.
- Create and export CSV files.
- Calculate and display relevant metrics accurately.
- Design a modular and maintainable code structure.
- Provide clear documentation on the assumed JSON format.
- Ensure the solution works without requiring edits.

---

### Additional Notes:

- Candidates should specify any requirements or dependencies for executing the script.
- Documentation or comments should clearly explain the assumed JSON format.
- Candidates can extend their solution to include additional functionality not listed in the prompt for extra marks. Be sure to document any extra features.

This scenario assesses the candidate's ability to work with JSON data, generate metrics, and create a robust, maintainable, and well-documented Python script.

---

# Example data:

data.json file and output.csv file are included in this example

## JSON file format:

As an example, an object of the JSON file is presented here:

{
    "id": "1",
    "name": "example_1",
    "test_result": "PASS",
    "start_date": "01-12-2023 12:00:00",
    "end_date": "01-12-2023 12:00:10"
}

Where:
- id: identifier of the JSON object
- name: name of the test associated
- test_result: result of test. Can be PASS or FAIL
- start_date: start of test execution in format: %d-%m-%Y %H:%M:%S
- end_date: end of test execution in format: %d-%m-%Y %H:%M:%S

---

## Additional libraries:

No additional libraries where required for this project.

Because of this, there is no requirements.txt file associated.

---

## How to execute

Run command:

```python
python main.py --json_path=<JSON_PATH> --csv_path=<CSV_PATH>
```

Where: 
- json_path = JSON input path to perform execution
- csv_path = CSV path where output will be created 